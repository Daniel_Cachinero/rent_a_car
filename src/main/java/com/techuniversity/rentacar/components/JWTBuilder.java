package com.techuniversity.rentacar.components;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.*;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.message.AuthException;

@Component
public class JWTBuilder {
    @Value("${jwt.issuer}")
    private String jwtIsuuer;
    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.expiry}")
    private Float jwtExpiry;
    RsaJsonWebKey rsaJsonWebKey;


    public JWTBuilder() {}

    //vamos a generar un token
    public String generateToken(String userId, String roles) {
        try {
            JwtClaims jwtClaims = new JwtClaims();
            jwtClaims.setIssuer(jwtIsuuer); //identificador del que solicita token
            jwtClaims.setExpirationTimeMinutesInTheFuture(jwtExpiry);
            jwtClaims.setAudience("ALL"); // puede invocarlo cualquiera
            jwtClaims.setStringListClaim("groups", roles); //grupo al q pertenece usuario
            jwtClaims.setGeneratedJwtId(); //para tener clave interna del token
            jwtClaims.setIssuedAtToNow(); //guarda hora de creación
            jwtClaims.setSubject("AUTHTOKEN");
            jwtClaims.setClaim("userID", userId); // Usuario que nos ha hecho petición

            //prepación de firma
            JsonWebSignature jws = new JsonWebSignature(); //herramienta que nos permita firmar
            jws.setPayload(jwtClaims.toJson()); //Carga del token y lo convierte a JSON
            jws.setKey(rsaJsonWebKey.getRsaPrivateKey()); //clave privada
            jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId()); //clave publica
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);//tipo de incriptación

            return jws.getCompactSerialization();

        } catch (JoseException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //creamos verificador de tokem
    public JwtClaims generateParseToken(String token) throws  Exception{
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setSkipSignatureVerification()
                .setAllowedClockSkewInSeconds(60) //valide expiracion cada 60 segundos
                .setRequireSubject()
                .setExpectedIssuer(jwtIsuuer)
                .setExpectedAudience("ALL")
                .setExpectedSubject("AUTHTOKEN")
                .setVerificationKey(rsaJsonWebKey.getKey())
                .setJwsAlgorithmConstraints(new AlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.WHITELIST, AlgorithmIdentifiers.RSA_USING_SHA256
                ))
                .build();
        try {
            JwtClaims jwtClaims = jwtConsumer.processToClaims(token);
            return jwtClaims;
        }catch (InvalidJwtException e) {
            try {
                if (e.hasExpired()) {
                    throw new Exception("Caducada en: " + e.getJwtContext().getJwtClaims().getExpirationTime());
                }
                if (e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID)) {
                    throw new Exception("Audiencia incorrecta: " + e.getJwtContext().getJwtClaims().getAudience());
                }
                throw new AuthException(e.getMessage());
            } catch (MalformedClaimException innerEx) {
                throw new AuthException("mal formada");
            }
        }

    }

    public String validarToken(String token) throws Exception{
        String userId = null;
        JwtClaims jwtClaims = generateParseToken(token);
        userId = jwtClaims.getClaimValue("userID").toString();
        return  userId;
    }

    @PostConstruct // se realiza después de ejecutarse el constructor por lo tanto tendremos los valores
    public void init(){
        try{
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);//generador de claves
            rsaJsonWebKey.setKeyId(jwtSecret); //introducción de clave
        }catch (JoseException ex){
            ex.printStackTrace();
        }
    }
}
