package com.techuniversity.rentacar.vehiculos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehiculoService{

    @Autowired
    VehiculoRepository vehiculoRepository;

    public List<VehiculoModel> findAll(){

        return vehiculoRepository.findAll();
    }

    public Optional<VehiculoModel> findByid(String id) {

        return vehiculoRepository.findById(id);
    }


    public VehiculoModel save(VehiculoModel vehiculo) {

        return vehiculoRepository.save(vehiculo);
    }

    public boolean deleteVehiculo(VehiculoModel vehiculo){
        try{
            vehiculoRepository.delete(vehiculo);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public List<VehiculoModel> findPaginado(int page){
        Pageable pageable = PageRequest.of(page, 5);
        Page<VehiculoModel> pages = vehiculoRepository.findAll(pageable);
        List<VehiculoModel> vehiculos = pages.getContent();
        return vehiculos;
    }


    public List<VehiculoModel> findPaginadoFiltro(int page, List<VehiculoModel> filtroCars ){
        int inicio = page*5;
        int fin;
        if (inicio + 5 > filtroCars.size()){
            fin = filtroCars.size();
        }else {
            fin = inicio + 5;
        }
        if (inicio > filtroCars.size()){
            return filtroCars.subList(0,0);
        }else{
            return filtroCars.subList(inicio,fin);
        }

    }

    public List<VehiculoModel> getVehiculoPrice(Double price1, Double price2) throws Exception {
        List<VehiculoModel> cars = vehiculoRepository.findAll();
        List<VehiculoModel> respuesta = new ArrayList<>();

        for (VehiculoModel car : cars) {
            if (car.getPriceDay() >= price1 && car.getPriceDay() <= price2) {
                respuesta.add(car);
            }
        }
        return respuesta;
    }

    public List<VehiculoModel> getVehiculoStock(boolean stock) {
        List<VehiculoModel> cars = vehiculoRepository.findAll();
        List<VehiculoModel> respuesta = new ArrayList<>();

        for (VehiculoModel car : cars) {
            if (car.isInStock() == (stock)) respuesta.add(car);
        }
        return respuesta;
    }

}
