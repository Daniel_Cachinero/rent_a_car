package com.techuniversity.rentacar.vehiculos;

import com.techuniversity.rentacar.vehiculos.VehiculoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VehiculoRepository extends MongoRepository<VehiculoModel, String> {

}