package com.techuniversity.rentacar.vehiculos;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "vehiculos")
public class VehiculoModel {

    @Id
    @NotNull
    private String id;
    private String brand;
    private String model;
    private String fuel;
    private String segment;
    private Double priceDay;
    private boolean inStock;

    public VehiculoModel() {
    }

    public VehiculoModel(String id, String brand, String model, String fuel, String segment, Double priceDay, boolean inStock) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.fuel = fuel;
        this.segment = segment;
        this.priceDay = priceDay;
        this.inStock = inStock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public Double getPriceDay() {
        return priceDay;
    }

    public void setPriceDay(Double priceDay) {
        this.priceDay = priceDay;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }
}
