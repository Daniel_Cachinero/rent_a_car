package com.techuniversity.rentacar.controllers;


import com.techuniversity.rentacar.components.JWTBuilder;
import com.techuniversity.rentacar.vehiculos.VehiculoModel;
import com.techuniversity.rentacar.vehiculos.VehiculoService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rentacar")
@CrossOrigin(origins = "*",methods = {RequestMethod.GET,RequestMethod.POST})
public class VehiculoController {

    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Techu")String name){
        return jwtBuilder.generateToken(name, "admin");
    }

    @Autowired
    VehiculoService vehiculoService;

    @GetMapping("/cars")
    public List<VehiculoModel> getVehiculos(@RequestParam(defaultValue = "-1") String page) {
        int iPage = Integer.parseInt(page);
        if (iPage == -1) {
            return vehiculoService.findAll();
        } else {
            return vehiculoService.findPaginado(iPage);
        }
    }

    @GetMapping("/cars/{id}")

    public ResponseEntity<VehiculoModel> getVehiculoId(@PathVariable String id) {
        Optional<VehiculoModel> respuesta = vehiculoService.findByid(id);
        if (respuesta.isPresent())  return new ResponseEntity<>(respuesta.get(), HttpStatus.OK);
        else return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/cars", headers = {"Authorization"})
    public ResponseEntity<VehiculoModel> insertVehiculo(@RequestBody VehiculoModel nuevo, @RequestHeader("Authorization") String token){
        String userId;
        try{
            userId = jwtBuilder.validarToken(token);
        }catch (Exception ex){
            System.out.println(ex);
            return ResponseEntity.badRequest().build();
        }
        vehiculoService.save(nuevo);
        return new ResponseEntity<>(nuevo, HttpStatus.CREATED);

    }
    @PutMapping("/cars")
    public ResponseEntity<Optional> updateVehiculo(@RequestBody VehiculoModel vehiculo){
        Optional<VehiculoModel> car = vehiculoService.findByid(vehiculo.getId());
        if (car.isPresent()) {
            vehiculoService.save(vehiculo);
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/cars")
    public ResponseEntity<Object> deleteVehiculo(@RequestBody VehiculoModel vehiculo){
        vehiculoService.deleteVehiculo(vehiculo);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/brand")
    public ResponseEntity<List<VehiculoModel>> getVehiculoBrand(@RequestParam String brand,@RequestParam(defaultValue = "-1") String page) {
        List<VehiculoModel> cars= vehiculoService.findAll();
        List <VehiculoModel> respuesta= new ArrayList<>();

        for (VehiculoModel car:cars){
            if (car.getBrand().equals(brand))respuesta.add(car);

        }
        if (respuesta.isEmpty()) {
            return ResponseEntity.notFound().build();
        }else{
            int iPage = Integer.parseInt(page);
            if (iPage == -1) {
                return new ResponseEntity<>(respuesta, HttpStatus.OK);
            } else {
                if (vehiculoService.findPaginadoFiltro(iPage,respuesta).isEmpty())
                return ResponseEntity.notFound().build();
                else
                return new ResponseEntity<>(vehiculoService.findPaginadoFiltro(iPage,respuesta), HttpStatus.OK);
            }

        }
    }

    @GetMapping("/price")
    public ResponseEntity<List<VehiculoModel>> getCarPrice(@RequestParam Double price1,@RequestParam Double price2,@RequestParam(defaultValue = "-1") String page) {
        try {
            List<VehiculoModel> prices = vehiculoService.getVehiculoPrice(price1, price2);
            if (prices.isEmpty()) {
                return ResponseEntity.notFound().build();
            } else {
                int iPage = Integer.parseInt(page);
                if (iPage == -1) {
                    return new ResponseEntity<>(prices, HttpStatus.OK);
                } else {
                    if (vehiculoService.findPaginadoFiltro(iPage, prices).isEmpty())
                        return ResponseEntity.notFound().build();
                    else
                        return new ResponseEntity<>(vehiculoService.findPaginadoFiltro(iPage, prices), HttpStatus.OK);
                }

            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/stock")
    public ResponseEntity<List<VehiculoModel>> getVehiculoStock(@RequestParam boolean stock,@RequestParam(defaultValue = "-1") String page) {
        List<VehiculoModel> stocks = vehiculoService.getVehiculoStock(stock);
        if (stocks.isEmpty()) {
            return ResponseEntity.notFound().build();
        }else{
            int iPage = Integer.parseInt(page);
            if (iPage == -1) {
                return new ResponseEntity<>(stocks, HttpStatus.OK);
            } else {
                if (vehiculoService.findPaginadoFiltro(iPage,stocks).isEmpty())
                    return ResponseEntity.notFound().build();
                else
                    return new ResponseEntity<>(vehiculoService.findPaginadoFiltro(iPage,stocks), HttpStatus.OK);
            }

        }
    }


}
